<?php

namespace App\Action\Log;

use App\Repository\LogRepositoryInterface;

class GetAllLogsAction
{
    private $logs;

    public function __construct(LogRepositoryInterface $logs)
    {
        $this->logs = $logs;
    }

    public function execute(): GetAllLogsResponse
    {
        return new GetAllLogsResponse($this->logs);
    }
}
