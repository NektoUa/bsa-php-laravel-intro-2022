<?php

namespace App\Action\Log;

class GetAllLogsResponse
{

    public $logs;

    public function __construct($logs)
    {
        $this->logs = $logs;
    }

    public function getLogs(): array
    {
        return $this->logs->findAll();
    }
}
