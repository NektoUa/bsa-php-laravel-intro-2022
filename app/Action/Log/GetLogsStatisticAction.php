<?php

namespace App\Action\Log;

use App\Repository\LogRepositoryInterface;
use App\Action\Log\GetLogsStatisticResponse;

class GetLogsStatisticAction
{
    private $logs;

    public function __construct(LogRepositoryInterface $logs)
    {
        $this->logs = $logs;
    }

    public function execute(): GetLogsStatisticResponse
    {
        return new GetLogsStatisticResponse($this->logs);
    }
}
