<?php

namespace App\Action\Log;

class GetLogsStatisticResponse
{
    private $logs;

    public function __construct($logs)
    {
        $this->logs = $logs;
    }

    public function getLogs(): array
    {
        return $this->logs->findAll();
    }
}
