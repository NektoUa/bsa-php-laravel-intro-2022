<?php

namespace App\Repository;

interface LogRepositoryInterface
{

    public function __construct(array $logs);

    public function findAll(): array;
}
