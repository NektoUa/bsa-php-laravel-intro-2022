<?php

namespace App\Repository;

class LogRepository implements LogRepositoryInterface
{
    public $logs;

    public function __construct(array $logs)
    {
        $this->logs = $logs;
    }

    public function findAll(): array
    {
        return $this->logs;
    }
}
