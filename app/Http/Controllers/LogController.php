<?php

namespace App\Http\Controllers;

use App\Action\Log\GetAllLogsResponse;
use App\Models\Log;
use Illuminate\Http\JsonResponse;

class LogController extends Controller
{
    public function getLogs()
    {
        $action = App::make(GetAllLogsResponse::class);
        $logs = $action->getLogs();
        return new JsonResponse($logs);
    }

    public function statistic()
    {
        $logs = new Log();
        return view('logs', ['rew' => $logs->all()]);
    }
}
