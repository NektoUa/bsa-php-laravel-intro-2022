<?php

namespace Database\Seeders;

use App\Models\Log;
use Faker\Factory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        \App\Models\Log::factory(10)->create();
    }
}
