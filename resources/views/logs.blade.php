<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Log Viewer</title>
</head>

<body>
    <h2>Log Viewer</h2>
    <ul>
        @if(isset($logs))
        @foreach($logs as $log)
        <li>{{ $log }}</li>
        @endforeach
        @endif
    </ul>
</body>

</html>